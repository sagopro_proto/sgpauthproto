PROTOC_FILE_MAIN = ./sgpauth.proto
PROTOC_FILE_ROLE = ./role.proto
PROTOC = protoc
PROTOC_FLAGS = -I . --go_out=. --go_opt=paths=source_relative \
							 --go-grpc_out=. --go-grpc_opt=paths=source_relative 

protoc_main:
	@echo "create sgpauth proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_MAIN)

protoc_role:
	@echo "create sgpauth role proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_ROLE)

all: protoc_role protoc_main

git:
	-git add . && git commit -m "__" && git push origin master

.PHONY: protoc_main protoc_role all git
